"""
This is a django-split-settings main file.

For more information read this:
https://github.com/sobolevn/django-split-settings
Default environment is `developement`.
To change settings file:
`DJANGO_ENV=production python manage.py runserver`
"""

from split_settings.tools import optional, include
from os import environ

VERSION = '0.1.0'
ENV = environ.get('DJANGO_ENV') or 'docker'

base_settings = [
    'components/common.py',  # standard django settings

    # You can even use glob:
    # 'components/*.py'

    # Select the right env:
    'environments/%s.py' % ENV,
    # Optionally override some settings:
    optional('environments/local.py'),
]

# Include settings:
include(*base_settings)
