from decouple import config


INSTALLED_APPS += ['django_s3_storage']

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = [
    config('AWS_LAMBDA_HOST'),
]

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': config('DB_NAME'),
        'USER': config('DB_USER'),
        'PASSWORD': config('DB_PASSWORD', default=''),
        'HOST': config('AWS_RDS_HOST'),
        'PORT': 5432,
    }
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

DEFAULT_FILE_STORAGE = 'django_s3_storage.storage.StaticS3Storage'
STATICFILES_STORAGE = 'django_s3_storage.storage.StaticS3Storage'
YOUR_S3_BUCKET = config('AWS_STORAGE_BUCKET_NAME')
AWS_S3_BUCKET_NAME_STATIC = YOUR_S3_BUCKET
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % YOUR_S3_BUCKET
STATIC_URL = "https://%s/" % AWS_S3_CUSTOM_DOMAIN
AWS_ACCESS_KEY_ID = config('AWS_ACCESS_KEY_ID')
AWS_SECRET_ACCESS_KEY = config('AWS_SECRET_ACCESS_KEY')

CORS_ORIGIN_WHITELIST = (
    'localhost:3000'
) 
