# SECURITY WARNING: don't run with debug turned on in production!

# Database
# https://docs.djangoproject.com/en/2.0/ref/settings/#databases
DEBUG = True

ALLOWED_HOSTS = [
    '127.0.0.1',
    'localhost'
]

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'postgres',
        'USER': 'postgres',
        'HOST': 'db',
        'PORT': 5432,
    }
}

DEFAULT_FILE_STORAGE = 'django.core.files.storage.FileSystemStorage'
STATIC_URL = '/static/'
STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'

CORS_ORIGIN_WHITELIST = (
    'localhost:3000'
)
